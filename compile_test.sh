export PATH="$coreutils/bin:$gcc/bin"

declare -xp

$cp -r $src/* .
$ls -la .

CXXFLAGS="-I $catch2/include -fsanitize=address" $make bin/tests

cp ./bin/tests $out
