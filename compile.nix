with import <nixpkgs>{};

derivation {
	name ="RPNCalc";
	
	make = "${pkgs.gnumake}/bin/make";
	ls = "${pkgs.coreutils}/bin/ls";
	cat = "${pkgs.coreutils}/bin/cat";
	cp = "${pkgs.coreutils}/bin/cp";
	coreutils = pkgs.coreutils;
	gcc = pkgs.gcc;

	src = ./.;	
	builder="${pkgs.bash}/bin/bash";
	args = [ ./compile.sh ];
	system=builtins.currentSystem;
}
