export PATH="$coreutils/bin:$afl/bin:$pgrep/bin"

mkdir -p ./test
mkdir -p ./out
$cp -r $src/test/in/* ./test/
$ls -la ./test
echo $fuzzer 
afl-fuzz -i ./test -o ./out $fuzzer & 
PID=$!
echo $PID
sleep 10 &&  kill $PID

mkdir -p $out
cp -r ./out $out

