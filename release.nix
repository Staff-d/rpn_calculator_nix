with import <nixpkgs>{};

{
	binary = import ./compile.nix;
	tests = import ./test.nix;
	afl = import ./afl.nix;
	runafl = import ./run_afl.nix;
}
