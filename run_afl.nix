with import <nixpkgs>{};

let 
	fuzzer = import ./afl.nix;

in derivation {
	name ="RPNRunAfl";
	
	make = "${pkgs.gnumake}/bin/make";
	ls = "${pkgs.coreutils}/bin/ls";
	cp = "${pkgs.coreutils}/bin/cp";
	coreutils = pkgs.coreutils;
	gcc = pkgs.gcc;
	afl = pkgs.afl;
	pgrep = pkgs.procps;
	
	src = ./.;
	builder="${pkgs.bash}/bin/bash";
	args = [ ./run_afl.sh ];
	system=builtins.currentSystem;
	
	inherit fuzzer;
}

