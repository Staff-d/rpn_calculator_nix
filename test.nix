with import <nixpkgs>{};

derivation {
	name ="RPNCalcTest";
	
	make = "${pkgs.gnumake}/bin/make";
	ls = "${pkgs.coreutils}/bin/ls";
	cat = "${pkgs.coreutils}/bin/cat";
	cp = "${pkgs.coreutils}/bin/cp";
	coreutils = pkgs.coreutils;
	gcc = pkgs.gcc;
	catch2=pkgs.catch2;

	src = ./.;	
	builder="${pkgs.bash}/bin/bash";
	args = [ ./compile_test.sh ];
	system=builtins.currentSystem;
}
