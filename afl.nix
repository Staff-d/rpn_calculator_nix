with import <nixpkgs>{};

derivation {
	name ="RPNAfl";
	
	make = "${pkgs.gnumake}/bin/make";
	ls = "${pkgs.coreutils}/bin/ls";
	cat = "${pkgs.coreutils}/bin/cat";
	cp = "${pkgs.coreutils}/bin/cp";
	coreutils = pkgs.coreutils;
	gcc = pkgs.gcc;
	afl = pkgs.afl;
	src = ./.;
	builder="${pkgs.bash}/bin/bash";
	args = [ ./compile_afl.sh ];
	system=builtins.currentSystem;
}

